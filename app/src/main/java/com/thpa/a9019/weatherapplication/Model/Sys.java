package com.thpa.a9019.weatherapplication.Model;

public class Sys {

    public int type;
    public int id;
    public double message;
    public int sunrise;
    public int sunset;

    public Sys(int type, int id, double message, int sunrise, int sunset) {
        this.type = type;
        this.id = id;
        this.message = message;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public int getSunrise() {
        return sunrise;
    }

    public void setSunrise(int sunrise) {
        this.sunrise = sunrise;
    }

    public int getSunset() {
        return sunset;
    }

    public void setSunset(int sunset) {
        this.sunset = sunset;
    }
}
