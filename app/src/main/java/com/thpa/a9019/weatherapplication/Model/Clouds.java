package com.thpa.a9019.weatherapplication.Model;

public class Clouds {
    public int all;

    public Clouds(int all) {
        this.all = all;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
