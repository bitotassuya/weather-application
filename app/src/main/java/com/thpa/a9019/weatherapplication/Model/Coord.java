package com.thpa.a9019.weatherapplication.Model;

public class Coord {
    private double lat;
    private double lon;

    public Coord(Double lat,Double lon){
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
