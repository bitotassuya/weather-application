package com.thpa.a9019.weatherapplication.Common;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Common {
    public static String API_KEY = "ae5d3eb277ae1c0b4353346f25d7b973";
    public static String API_LINK = "http://api.openweathermap.org/data/2.5/weather";

    @NonNull
    public static String apiRequest(String lat, String lng) {
        StringBuilder sb = new StringBuilder(API_LINK);
        sb.append(String.format("city?lat=%s&lon=%s&APPID=%as&units=metric", lat, lng, API_KEY));
        return sb.toString();

    }

    public static String unixTimeStampToDateTime(double uniitTimeStamp) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        date.setTime((long)uniitTimeStamp*1000);
        return dateFormat.format(date);
    }
    public static  String getImage(String icon){
        return String.format("http://http://api.openweathermap.org/img/w/%s.png",icon);
    }
    public static String getDateNow(){
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
